`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: cpu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module cpu
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_OPERAND      =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del operando
    parameter               NB_SELECT_A     =   2,                        // Longitud del select mux A
    parameter               NB_SELECT_B     =   1,                        // Longitud del select mux B
    parameter               NB_CODE         =   6,                        // Longitud del codigo de op ALU
    parameter               NB_ADDRESS      =   4                         // Longitud de direccion del programa
)
(
    // INPUTS   
    input wire                              i_clk,
    input wire                              i_reset,
    
    // PROGRAM MEMORY
    input wire  [NB_INSTRUCTION-1:0]        i_pm_data,
    output wire [NB_ADDRESS-1:0]            o_pm_addr,
    
    // DATA MEMORY
    input wire  [NB_INSTRUCTION-1:0]        i_dm_data,
    output wire [NB_INSTRUCTION-1:0]        o_dm_data,
    output wire [NB_OPERAND-1:0]            o_dm_addr,
    output wire                             o_rd,
    output wire                             o_wr
);

    // INTERNAL
    wire        [NB_SELECT_A-1:0]           select_A;
    wire        [NB_SELECT_B-1:0]           select_B;
    wire                                    enable;
    wire        [NB_CODE-1:0]               op;
    
    control
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A)
    )
    u_control
    (
        .i_pm_data          (i_pm_data), 
        .i_clk              (i_clk),
        .i_reset_pc         (i_reset),
        .o_selA             (select_A),
        .o_selB             (select_B),
        .o_wrACC            (enable),
        .o_op               (op),
        .o_wrRAM            (o_wr),
        .o_rdRAM            (o_rd),
        .o_operand          (o_dm_addr), 
        .o_address          (o_pm_addr)    
    );
    
    datapath
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A),
        .NB_SELECT_B        (NB_SELECT_B),
        .NB_CODE            (NB_CODE)    
    )
    u_datapath
    (
        .i_clk              (i_clk),
        .i_enable           (enable),
        .i_reset            (i_reset),
        .i_se               (o_dm_addr),
        .i_select_A         (select_A),
        .i_select_B         (select_B),
        .i_op               (op),
        .i_dm               (i_dm_data),
        .o_acc              (o_dm_data)
    );

endmodule