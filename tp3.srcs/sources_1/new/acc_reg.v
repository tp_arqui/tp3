`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: acc_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module acc_reg
#(
    parameter               NB_INSTRUCTION  =   16                        // Longitud de la instruccion
)
(
    // INPUTS   
    input wire                              i_clk,
    input wire                              i_enable,
    input wire                              i_reset,
    input wire [NB_INSTRUCTION-1:0]         i_acc,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]        o_acc
);

    // INTERNAL
    reg [NB_INSTRUCTION-1:0]                acc_reg, acc_next; 
    
    always @(posedge i_clk) begin
        if (i_reset) acc_reg <= {NB_INSTRUCTION{1'b0}};
        else if (i_enable) acc_reg <= acc_next;
    end
    
    always @(*) begin
        acc_next <= i_acc;
    end
    
    // OUTPUT
    assign o_acc = acc_reg;

endmodule