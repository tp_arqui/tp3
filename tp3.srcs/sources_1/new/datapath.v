`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module datapath
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_OPERAND      =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del operando        
    parameter               NB_SELECT_A     =   2,                        // Longitud del select mux A
    parameter               NB_SELECT_B     =   1,                        // Longitud del select mux B
    parameter               NB_CODE         =   6                         // Longitud del codigo de op ALU
)
(
    // INPUTS   
    // ACC_REG
    input wire                              i_clk,
    input wire                              i_enable,
    input wire                              i_reset,
    
    // SIGNAL_EXTENSION
    input wire [NB_OPERAND-1:0]             i_se,
    
    // MUX_A
    input wire [NB_SELECT_A-1:0]            i_select_A,
    
    // MUX_B
    input wire [NB_SELECT_B-1:0]            i_select_B,
    
    // ALU
    input wire [NB_CODE-1:0]                i_op, 
    
    // DATA_MEMORY
    input wire [NB_INSTRUCTION-1:0]         i_dm,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]        o_acc
);

    // INTERNAL
    wire        [NB_INSTRUCTION-1:0]        o_se;
    wire        [NB_INSTRUCTION-1:0]        o_alu;
    wire        [NB_INSTRUCTION-1:0]        o_mux_A;
    wire        [NB_INSTRUCTION-1:0]        o_mux_B;
    
    signal_extension
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND)
    )
    u_signal_extension
    (
        .i_se               (i_se),
        .o_se               (o_se)
    );
    
    mux_A
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_SELECT          (NB_SELECT_A)
    )
    u_mux_A
    (
        .i_select           (i_select_A),
        .i_dm               (i_dm),
        .i_se               (o_se),
        .i_op               (o_alu),
        .o_mux              (o_mux_A)
    );
    
    mux_B
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_SELECT          (NB_SELECT_B)
    )
    u_mux_B
    (
        .i_select           (i_select_B),
        .i_dm               (i_dm),
        .i_se               (o_se),
        .o_mux              (o_mux_B)
    );
    
    acc_reg
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION)
    )
    u_acc_reg
    (
        .i_clk              (i_clk),
        .i_enable           (i_enable),
        .i_reset            (i_reset),
        .i_acc              (o_mux_A),
        .o_acc              (o_acc)
    );
    
    alu
    #(
        .NB_DATA            (NB_INSTRUCTION),
        .NB_DATA_OUT        (NB_INSTRUCTION)
    )
    u_alu
    (
        .i_a        (o_acc),
        .i_b        (o_mux_B),
        .i_op       (i_op),
        .o_r        (o_alu)
    );    
    
endmodule