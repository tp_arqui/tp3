`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: mux_A
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module mux_A
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_SELECT       =   2                         // Longitud del select
)
(
    // INPUTS   
    input wire [NB_SELECT-1:0]              i_select,
    input wire [NB_INSTRUCTION-1:0]         i_dm,
    input wire [NB_INSTRUCTION-1:0]         i_se,
    input wire [NB_INSTRUCTION-1:0]         i_op,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]        o_mux
);

    // LOCAL_PARAMETERS
    localparam  DM = 2'b00;         // Data memory
    localparam  SE = 2'b01;         // Signal extension
    localparam  OP = 2'b10;         // Operation output
    
    // INTERNAL
    reg         [NB_INSTRUCTION-1:0]       aux = 0; 
    
    always @(*) begin
        case (i_select)
            DM: aux <= i_dm;
            SE: aux <= i_se;
            OP: aux <= i_op;
            default: aux <= 0;
        endcase
    end
    
    // OUTPUT
    assign o_mux = aux;
    
endmodule