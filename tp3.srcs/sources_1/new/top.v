`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module top
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_OPERAND      =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del operando
    parameter               NB_SELECT_A     =   2,                        // Longitud del select mux A
    parameter               NB_SELECT_B     =   1,                        // Longitud del select mux B
    parameter               NB_CODE         =   6,                        // Longitud del codigo de op ALU
    parameter               NB_DATA         =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del dato
    parameter               RAM_SIZE        =   NB_INSTRUCTION,           // Tama?o de la memoria (16 palabras)   
    parameter               NB_ADDRESS      =   4 	                        //number of addressbits
)
(
    // INPUTS   
    input wire                              i_clk,
    input wire                              i_reset,
    
    // OUTPUTS
    output wire                             o_tx
);
    
    // INTERNAL
    wire                                    tx_start;
    wire [NB_INSTRUCTION-1:0]               o_acc;

    uart
    #(
        .NB_DATA          (NB_INSTRUCTION),
        .SB_TICK          (NB_INSTRUCTION)
    )
    u_uart
    (
        .i_clk            (i_clk),
        .i_reset          (i_reset), 
        .i_tx_start       (tx_start),
        .i_tx             (o_acc),
        .o_tx             (o_tx),
        .o_tx_done        (o_tx_done)
    );

    bip
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A),
        .NB_SELECT_B        (NB_SELECT_B),
        .NB_CODE            (NB_CODE),
        .NB_DATA            (NB_DATA),
        .RAM_SIZE           (RAM_SIZE),
        .NB_ADDRESS         (NB_ADDRESS)
    )
    u_bip
    (
        .i_clk              (i_clk),
        .i_reset            (i_reset),
        .o_acc              (o_acc),
        .o_tx_start         (tx_start)
    );
endmodule  