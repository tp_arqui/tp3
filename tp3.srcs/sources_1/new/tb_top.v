`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/25/2020 07:04:22 PM
// Design Name: 
// Module Name: tb_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_top();

    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_OPERAND       = NB_INSTRUCTION-NB_OPCODE;        
    localparam NB_SELECT_A      = 2;
    localparam NB_SELECT_B      = 1;
    localparam NB_CODE          = 6;
    localparam NB_DATA          = NB_INSTRUCTION-NB_OPCODE;
    localparam NB_ADDRESS       = 4;
    localparam RAM_SIZE         = NB_INSTRUCTION;
    localparam RESULT           = 16'hffd8;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         clk_reset;
    reg                         top_reset;
    // CLK_WIZ
    wire                        locked;
    wire                        out1;
    // TOP
    wire                        o_tx;
    // UART
    wire [NB_INSTRUCTION-1:0]   o_rx;
    wire                        o_rx_done;
    

    initial begin
        #10
        clk = 1'b0;
        clk_reset = 1'b0;
        top_reset = 1'b1;
                
        #10
        clk_reset = 1'b1;
                        
        while(~locked) begin
            #10
            clk_reset = 1'b0;
        end
        
        #100
        top_reset = 1'b1;
        
        #10
        top_reset = 1'b0;
        
        #2000000
        
        $display("############# Test OK ############");
        $finish();
                
    end

    clk_wiz_0 uut
    (
        .clk_out1           (out1),
        .reset              (clk_reset),
        .locked             (locked),
        .clk_in1            (clk)        
    );
    
    top
    #(
       .NB_INSTRUCTION     (NB_INSTRUCTION),
       .NB_OPCODE          (NB_OPCODE),
       .NB_OPERAND         (NB_OPERAND),
       .NB_SELECT_A        (NB_SELECT_A),
       .NB_SELECT_B        (NB_SELECT_B),
       .NB_CODE            (NB_CODE),
       .NB_DATA            (NB_DATA),
       .RAM_SIZE           (RAM_SIZE),
       .NB_ADDRESS         (NB_ADDRESS)
    )
    
    u_top
    (
       .i_clk              (out1),
       .i_reset            (top_reset),
       .o_tx               (o_tx)
    );
    
    uart
    #(
        .NB_DATA          (NB_INSTRUCTION),
        .SB_TICK          (NB_INSTRUCTION)
    )
    u_uart
    (
        .i_clk            (out1),
        .i_reset          (top_reset), 
        .i_rx             (o_tx), 
        .o_rx_done        (o_rx_done),
        .o_rx             (o_rx)
    );
        
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    // TEST
    always @(posedge clk) begin       
        if (o_rx_done) begin
            if(o_rx != RESULT) begin
                $display("############# Test FALLO ############");
                $finish();
            end
        end        
    end

endmodule
