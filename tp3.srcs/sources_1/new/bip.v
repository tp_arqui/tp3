`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: bip
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module bip
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_OPERAND      =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del operando
    parameter               NB_SELECT_A     =   2,                        // Longitud del select mux A
    parameter               NB_SELECT_B     =   1,                        // Longitud del select mux B
    parameter               NB_CODE         =   6,                        // Longitud del codigo de op ALU
    parameter               NB_DATA         =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del dato
    parameter               RAM_SIZE        =   NB_INSTRUCTION,           // Tama?o de la memoria (16 palabras)   
    parameter               NB_ADDRESS      =   4 	                        //number of addressbits
)
(
    // INPUTS   
    input wire                              i_clk,
    input wire                              i_reset,
    
    // OUTPUTS
    output wire [NB_INSTRUCTION-1:0]        o_acc,
    output wire                             o_tx_start
);    
    
    // LOCALPARAMETER
    localparam                              HLT = 5'b00000;
    
    // INTERNAL
    // AUX
    reg                                     finish;
    
    // PROGRAM MEMORY
    wire  [NB_INSTRUCTION-1:0]              pm_data;
    wire  [NB_ADDRESS-1:0]                  pm_addr;
    
    // DATA MEMORY
    wire  [NB_INSTRUCTION-1:0]              dm_data_input;
    wire  [NB_INSTRUCTION-1:0]              dm_data_output;
    wire  [NB_OPERAND-1:0]                  dm_addr;
    wire                                    rd;
    wire                                    wr;
    
    cpu
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A),
        .NB_SELECT_B        (NB_SELECT_B),
        .NB_CODE            (NB_CODE),
        .NB_ADDRESS         (NB_ADDRESS)
    )
    u_cpu
    (
        .i_clk              (i_clk),
        .i_reset            (i_reset),
        .i_pm_data          (pm_data),
        .o_pm_addr          (pm_addr),
        .i_dm_data          (dm_data_input),
        .o_dm_data          (dm_data_output),
        .o_dm_addr          (dm_addr),
        .o_rd               (rd),
        .o_wr               (wr)        
    );

    data_memory
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_DATA            (NB_OPERAND),
        .RAM_SIZE           (RAM_SIZE) 
    )
    u_data_memory
    (
        .i_clk              (i_clk),
        .i_wr               (wr),
        .i_rd               (rd),
        .i_addr             (dm_addr),
        .i_data             (dm_data_output),
        .o_data             (dm_data_input)
    );
    
    program_memory
    #(
        .NB_DATA     (NB_INSTRUCTION),
        .NB_ADDRESS  (NB_ADDRESS)
    )
    u_program_memory(
        .i_addr      (pm_addr),
        .o_data      (pm_data)
    );
    
    always @(posedge i_clk) begin
        if (pm_data == HLT)
            finish <= 1'b1;
        else
            finish <= 1'b0;
    end
        
    assign o_tx_start = (pm_data==HLT) && ~finish;
    
    assign o_acc = dm_data_output;
    
endmodule