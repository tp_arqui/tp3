`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/23/2020 04:58:27 PM
// Design Name: 
// Module Name: tb_instruction_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_instruction_decoder();

        // LOCAL_PARAMETERS
        localparam NB_OPCODE           = 5;
        localparam NB_SELECT_A         = 2;
        localparam NB_CODE             = 6;
        //OPCODES
        localparam HLT_OPCODE          = 5'b00000; 
        localparam STO_OPCODE          = 5'b00001; 
        localparam LD_OPCODE           = 5'b00010; 
        localparam LDI_OPCODE          = 5'b00011; 
        localparam ADD_OPCODE          = 5'b00100; 
        localparam ADDI_OPCODE         = 5'b00101; 
        localparam SUB_OPCODE          = 5'b00110; 
        localparam SUBI_OPCODE         = 5'b00111; 
        localparam FAIL_OPCODE         = 5'b01000;      //Para probar con un opcode invalido 
        
        // LOCAL_PARAMETERS
        localparam  ADD_ALU = 6'b100000;
        localparam  SUB_ALU = 6'b100010;
        
        //INSTRUCTION_DECODER
        reg  [NB_OPCODE - 1 : 0]        i_opcode;
        wire                            o_wrPC;
        wire [1:0]                      o_selA;
        wire                            o_selB;
        wire                            o_wrACC;
        wire [NB_CODE-1:0]              o_op;
        wire                            o_wrRAM;
        wire                            o_rdRAM;
        
        initial
            begin
                i_opcode = HLT_OPCODE;
                
                #10
                if(o_wrPC != 0 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = STO_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 1 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = LD_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = LDI_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b01 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = ADD_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b10 || o_selB != 0 || o_wrACC != 1 || o_op != ADD_ALU || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = ADDI_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b10 || o_selB != 1 || o_wrACC != 1 || o_op != ADD_ALU || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = SUB_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b10 || o_selB != 0 || o_wrACC != 1 || o_op != SUB_ALU || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = SUBI_OPCODE;
                #5
                if(o_wrPC != 1 || o_selA != 2'b10 || o_selB != 1 || o_wrACC != 1 || o_op != SUB_ALU || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                    
                #5
                i_opcode = FAIL_OPCODE;
                #5
                if(o_wrPC != 0 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
                
            end 
            
            instruction_decoder
            #(
                .NB_OPCODE          (NB_OPCODE),
                .NB_SELECT_A        (NB_SELECT_A)
            )
            u_instruction_decoder
            (
                .i_opcode           (i_opcode),
                .o_wrPC             (o_wrPC),
                .o_selA             (o_selA),
                .o_selB             (o_selB),
                .o_wrACC            (o_wrACC),
                .o_op               (o_op),
                .o_wrRAM            (o_wrRAM),
                .o_rdRAM            (o_rdRAM)
            );
endmodule
