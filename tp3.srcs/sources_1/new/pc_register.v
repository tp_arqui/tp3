module pc_register
#(
	parameter B = 4	// number of bits
)
(
	input wire i_clk, i_enable,i_reset,
	input wire [3:0] i_pc, // d
	output wire [3:0] o_pc // q
);

reg [3:0] pc_reg=0, pc_next=0; // la direccion inicial en 0

//body
always@(posedge i_clk)
begin
	if (i_reset)
	   pc_reg<=0;
    else if (i_enable)   
        pc_reg<=pc_next;
end

always @(*)
    pc_next = i_pc;              //Cuando cambia pc_in, en el proximo clock cambio el pc_out. Y cambio la direccion
    

    assign o_pc = pc_reg;	

endmodule
