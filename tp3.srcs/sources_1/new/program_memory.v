module program_memory
#(
	parameter 	NB_DATA     = 16,	//number of bits
			    NB_ADDRESS  = 4 	//number of addressbits
)
(
	input wire [NB_ADDRESS-1:0] i_addr,      //Nuestro program memory es de solo 16 registros, por eso la direccion de 4 bits.
	output wire [NB_DATA-1:0] o_data
);

//signal declaration
reg[NB_DATA-1:0]array_reg[2**NB_ADDRESS-1:0];

//-- Inicializacion de la memoria. 
//-- Solo se dan valores a las 9 primeras posiciones
//-- El resto permanecera a 0
  initial begin
    array_reg[0] = 16'h180A; //LDI 10; ACC<-10              OpCode = 00011 - Operand = 00000001010 
    array_reg[1] = 16'h0800; //STO A ;A <- ACC; address     OpCode = 00001 - Operand = 00000000000  //Toma el valor de ACC y lo guarda en memoria 
    array_reg[2] = 16'h1814; //LDI 20;  ACC<-20 
    array_reg[3] = 16'h0801; //STO B; B <- ACC 
    array_reg[4] = 16'h1000; //LD A; ACC<- A 
    array_reg[5] = 16'h2001; //ADD B; ACC<-ACC+B 
    array_reg[6] = 16'h3805; //SUBI 5; ACC<-ACC-5
    array_reg[7] = 16'h0802; //STO C; C<-ACC 
	array_reg[8] = 16'h1FF1; // LDI -15; ACC<- -15 
    array_reg[9] = 16'h3002; //	SUB C; ACC<-ACC-C
    array_reg[10] = 16'h0000; //
    array_reg[11] = 16'h0000; //
    array_reg[12] = 16'h0000; //
    array_reg[13] = 16'h0000; //
    array_reg[14] = 16'h0000; //
    array_reg[15] = 16'h0000; //
   end

//read operation
assign o_data = array_reg[i_addr];

endmodule
