`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/20/2020 07:33:34 PM
// Design Name: 
// Module Name: tb_datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_datapath();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_OPERAND       = NB_INSTRUCTION-NB_OPCODE;        
    localparam NB_SELECT_A      = 2;
    localparam NB_SELECT_B      = 1;
    localparam NB_CODE          = 6;
    
    // AUX
    localparam DATA1            = 11'h2AA;
    localparam DATA2            = ~DATA1;
    localparam DM               = 2'b00;         // Data memory
    localparam SE               = 2'b01;         // Signal extension
    localparam OP               = 2'b10;         // Operation output
    localparam ADD              = 6'b100000;
    localparam SUB              = 6'b100010;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         test_start;
    // ACC_REG
    reg                         enable;
    reg                         reset;
    // SIGNAL_EXTENSION
    reg [NB_OPERAND-1:0]        i_se;
    // MUX_A
    reg [NB_SELECT_A-1:0]       i_select_A;
    // MUX_B
    reg [NB_SELECT_B-1:0]       i_select_B;
    // ALU
    reg [NB_CODE-1:0]           i_op;
    // DATA_MEMORY
    reg [NB_INSTRUCTION-1:0]    i_dm;
    // ACC_OUT
    wire [NB_INSTRUCTION-1:0]   o_acc;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        enable = 1'b0;
        reset = 1'b0;
        
        i_se = {NB_OPERAND{1'b0}};
        i_select_A = {NB_SELECT_A{1'b0}};
        i_select_B = {NB_SELECT_B{1'b0}};
        i_op = {NB_CODE{1'b0}};
        i_dm = {NB_INSTRUCTION{1'b0}};
        
        #10
        test_start = 1'b1;
        reset = 1'b1;
        
        #10
        reset = 1'b0;
        
        #50
        i_se = DATA1;
        i_select_A = SE;
        i_select_B = SE;
        i_op = ADD;
        
        // PRUEBA DE SALIEDA SIGNAL EXTENSION
        #20
        enable = 1'b1;
        
        #30
        if (o_acc != {5'h00,DATA1}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        enable = 1'b0;
        i_se = DATA2;
        
        #20
        enable = 1'b1;
        
        #30
        if (o_acc != {5'hFF,DATA2}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        // PRUEBA DE SALIDA OP CON MUX_B LEYENDO DATA MEMORY
        #10
        enable = 1'b0;
        i_dm = DATA1;
        i_select_A = OP;
        i_select_B = DM;
                
        #20
        enable = 1'b1;
        
        #10
        if (o_acc != {5'hFF,(DATA1+DATA2)}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        enable = 1'b0;
        i_op = SUB;
                
        #20
        enable = 1'b1;
        
        #10
        if (o_acc != {5'hFF,DATA2}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        // PRUEBA DE SALIDA OP CON MUX_B LEYENDO SIGNAL EXTENSION
        #10
        enable = 1'b0;
        i_se = DATA1;
        i_op = ADD;
        i_select_B = SE;
                
        #20
        enable = 1'b1;
        
        #10
        if (o_acc != {5'hFF,(DATA1+DATA2)}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        enable = 1'b0;
        i_op = SUB;
                
        #20
        enable = 1'b1;
        
        #10
        if (o_acc != {5'hFF,DATA2}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        // PRUEBA DE SALIDA DM
        #10
        enable = 1'b0;
        i_dm = DATA1;
        i_select_A = DM;
                
        #20
        enable = 1'b1;
        
        #10
        if (o_acc != {5'h00,DATA1}) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #1000
        
        $display("############# Test OK ############");
        $finish();
                
    end

    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    datapath
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A),
        .NB_SELECT_B        (NB_SELECT_B),
        .NB_CODE            (NB_CODE)    
    )
    u_datapath
    (
        .i_clk              (clk),
        .i_enable           (enable),
        .i_reset            (reset),
        .i_se               (i_se),
        .i_select_A         (i_select_A),
        .i_select_B         (i_select_B),
        .i_op               (i_op),
        .i_dm               (i_dm),
        .o_acc              (o_acc)
    );
endmodule
