`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: data_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module data_memory
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_DATA         =   NB_INSTRUCTION-NB_OPCODE, // Longitud en bits del dato
    parameter               RAM_SIZE        =   NB_INSTRUCTION            // Tama?o de la memoria (16 palabras)       
)
(
    // INPUTS
    input wire                              i_clk,
    input wire                              i_wr,
    input wire                              i_rd,   
    input wire [NB_DATA-1:0]                i_addr,
    input wire [NB_INSTRUCTION-1:0]         i_data,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]        o_data
);

    reg         [NB_INSTRUCTION-1:0]        ram[RAM_SIZE-1:0];            // Array de registros de 2048 de largo y 16 de ancho
    
    always @(negedge i_clk) begin                                         // Realizo la escritura durante el semiciclo positivo
        if (i_wr)                                                         // del clock y la escritura se encuentre habilitada
            ram[i_addr] <= i_data;                                        // 
    end
    
    // OUTPUT                                                             // El dato se lee al instante y se asigna a la salida
    assign o_data = ram[i_addr];                                          // De igual modo cuando se escribe se disponibiliza
                                                                          // apenas se escribe (necesario para monociclo)   
endmodule