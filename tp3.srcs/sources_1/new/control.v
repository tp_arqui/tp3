module control
#(
    parameter NB_INSTRUCTION = 16, 
              NB_OPCODE      = 5,
              NB_OPERAND     = NB_INSTRUCTION - NB_OPCODE,
              NB_SELECT_A    = 2,
              NB_CODE        = 6          
)
(
	input  wire [NB_INSTRUCTION - 1:0]     i_pm_data,
	input  wire                            i_clk,
	input  wire                            i_reset_pc,
	output wire [NB_SELECT_A - 1:0]        o_selA,
	output wire                            o_selB,
	output wire                            o_wrACC,
	output wire [NB_CODE-1:0]              o_op,
	output wire                            o_wrRAM,
	output wire                            o_rdRAM,
	output wire [NB_OPERAND - 1:0]         o_operand,
	output wire [3:0]                      o_address  
);
 
// wire enable_pc;
wire [3:0]      pc_in, pc_out; //Les pongo un nombre diferente  a i_pc y o_pc, para diferenciarlos de los wire de los modulos.
wire            enable_pc;

instruction_decoder u_instruction_decoder
(
	.i_opcode          (i_pm_data[NB_INSTRUCTION - 1 : NB_OPERAND]),             //Los 5 bits mas altos. 
	.o_wrPC            (enable_pc),
	.o_selA            (o_selA),
	.o_selB            (o_selB),
	.o_wrACC           (o_wrACC),
	.o_op              (o_op),
	.o_wrRAM           (o_wrRAM),
	.o_rdRAM           (o_rdRAM)
);

pc_register u_pc_register
(
	.i_clk(i_clk), 
	.i_enable(enable_pc),
	.i_pc(pc_in),  
	.i_reset(i_reset_pc),
	.o_pc(o_address)
);

pc_adder u_pc_adder
(
	.i_pc(o_address), 
	.o_pcadd(pc_in)
);

assign o_operand=i_pm_data[ NB_OPERAND - 1:0];

endmodule
