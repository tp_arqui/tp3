`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/23/2020 04:10:37 PM
// Design Name: 
// Module Name: tb_program_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_program_memory();

     // LOCAL_PARAMETERS
    localparam NB_DATA           = 16;
    localparam NB_ADDRESS        = 4;
    
    //AUX
    //Programa en la memoria
    localparam DATA_1           = 16'h180A; //LDI 10; ACC<-10
    localparam DATA_2           = 16'h0800; //STO A ;A <- ACC;
    localparam DATA_3           = 16'h1814; //LDI 20;  ACC<-20
    localparam DATA_4           = 16'h0801; //STO B; B <- ACC
    localparam DATA_5           = 16'h1000; //LD A; ACC<- A 
    localparam DATA_6           = 16'h2001; //ADD B; ACC<-ACC+B
    localparam DATA_7           = 16'h3805; //SUBI 5; ACC<-ACC-5
    localparam DATA_8           = 16'h0802; //STO C; C<-ACC                
    localparam DATA_9           = 16'h1FF1; // LDI -15; ACC<- -15  
    localparam DATA_10          = 16'h3002; //	SUB C; ACC<-ACC-C
    localparam DATA_11          = 16'h0000; //	No hay mas programa a partir de esta direccion
    
    //Direcciones de la  memoria de programa
    localparam ADDRESS_0        = 4'b0000;
    localparam ADDRESS_1        = 4'b0001;
    localparam ADDRESS_2        = 4'b0010;
    localparam ADDRESS_3        = 4'b0011;
    localparam ADDRESS_4        = 4'b0100;
    localparam ADDRESS_5        = 4'b0101;
    localparam ADDRESS_6        = 4'b0110;
    localparam ADDRESS_7        = 4'b0111;
    localparam ADDRESS_8        = 4'b1000;
    localparam ADDRESS_9        = 4'b1001;
    localparam ADDRESS_10        = 4'b1011;
    
    // TB_SIGNALS                          
    reg                         test_start;
    reg [NB_ADDRESS-1 : 0]      address;
    wire [NB_DATA - 1 : 0]      data;
    
    initial begin
        test_start = 1'b0;
        
        
        #10
        test_start = 1'b1;
        address = ADDRESS_0;
        if (data != DATA_1) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_1;
        if (data != DATA_2) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_2;
        if (data != DATA_3) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        #10
        address = ADDRESS_3;
        if (data != DATA_4) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_4;
        if (data != DATA_5) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_5;
        if (data != DATA_6) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_6;
        if (data != DATA_7) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_7;
        if (data != DATA_8) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_8;
        if (data != DATA_9) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_9;
        if (data != DATA_10) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        address = ADDRESS_10;
        if (data != DATA_11) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
    end
    
    
    program_memory
    #(
        .NB_DATA     (NB_DATA),
        .NB_ADDRESS  (NB_ADDRESS)
    )
    u_program_memory(
        .i_addr      (address),
        .o_data      (data)
    );
endmodule
