`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/20/2020 05:33:41 PM
// Design Name: 
// Module Name: tb_acc_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_acc_reg();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam DATA             = 16'hAAAA;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         test_start;
    // ACC_REG
    reg                         enable;
    reg                         reset;
    reg [NB_INSTRUCTION-1:0]    i_acc;
    wire [NB_INSTRUCTION-1:0]   o_acc;
 

    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        enable = 1'b0;
        reset = 1'b0;
        
        #10
        test_start = 1'b1;
        reset = 1'b1;
        
        #10
        reset = 1'b0;
        
        #100
        i_acc = DATA;
        
        #100
        enable = 1'b1;
        
        #10
        enable = 1'b0;
        
        #1000
        
        $display("############# Test OK ############");
        $finish();
                
    end
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    acc_reg
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION)
    )
    u_acc_reg
    (
        .i_clk              (clk),
        .i_enable           (enable),
        .i_reset            (reset),
        .i_acc              (i_acc),
        .o_acc              (o_acc)
    );
    
    
    // TEST
    always @(posedge clk) begin       
        if (test_start) begin
            if (reset && (o_acc != {NB_INSTRUCTION{1'b0}})) begin
                $display("############# Test FALLO ############");
                $finish();
            end
            else if (enable && (o_acc != DATA)) begin
                $display("############# Test FALLO ############");
                $finish();
            end
        end        
    end
endmodule
