`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: signal_extension
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module signal_extension
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_OPCODE       =   5,                        // Longitud del opcode
    parameter               NB_OPERAND      =   NB_INSTRUCTION-NB_OPCODE  // Longitud en bits del operando        
)
(
    // INPUTS   
    input wire [NB_OPERAND-1:0]            i_se,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]       o_se
);
    
    // INTERNAL
    reg         [NB_INSTRUCTION-1:0]       aux = 0;            

    always @(*) begin                                   // Combinacional
        if (i_se[NB_OPERAND-1])                         // Si MSB del operando es '1' concateno
            aux <= {5'h1F, i_se};                       // '1' en los 5 bits mas altos del opcode
        else                                            // Si MSB=0, concateno '0'
            aux <= {5'h00, i_se};
    end
    
    // OUTPUT
    assign o_se = aux;
    
endmodule
