`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/21/2020 03:58:26 PM
// Design Name: 
// Module Name: tb_data_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_data_memory();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_DATA          = NB_INSTRUCTION-NB_OPCODE;        
    localparam RAM_SIZE         = NB_INSTRUCTION;
    
    // AUX
    localparam DATA1            = 16'hAAAA;
    localparam DATA2            = ~DATA1;
    localparam ADDR1            = 4'h1;
    localparam ADDR2            = 4'h2;
    
    localparam HIGH             = 1'b1;
    localparam LOW              = ~HIGH;
    
    // TB_SIGNALS                          
    reg                         clk;       
    reg                         test_start;
    // DATA_MEMORY
    reg                         i_wr;
    reg                         i_rd;
    reg [NB_DATA-1:0]           i_addr;
    reg [NB_INSTRUCTION-1:0]    i_data;
    wire [NB_INSTRUCTION-1:0]   o_data;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        i_wr = 1'b0;
        i_rd = 1'b0;
        
        i_addr = {NB_DATA{1'b0}};
        i_data = {NB_INSTRUCTION{1'b0}};
        
        #10
        test_start = 1'b1;

        #50
        // PRUEBA ESCRITURA DE DATA1 EN ADDR1
        i_addr = ADDR1;
        i_data = DATA1;
        
        #20
        i_wr = HIGH;
        
        #10
        i_wr = LOW;
        
        #20
        // PRUEBA DE LECTURA EN ADDR1
        i_rd = HIGH;
        
        #10
        if (o_data != DATA1) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        i_rd = LOW;
        
        #50
        // PRUEBA ESCRITURA DE DATA2 EN ADDR2
        i_addr = ADDR2;
        i_data = DATA2;
        
        #20
        i_wr = HIGH;
        
        #10
        i_wr = LOW;
        
        #20
        // PRUEBA DE LECTURA EN ADDR2
        i_rd = HIGH;
        
        #10
        if (o_data != DATA2) begin
            $display("############# Test FALLO ############");
            $finish();
        end
        
        #10
        i_rd = LOW;
                
        #1000
        
        $display("############# Test OK ############");
        $finish();
                
    end

    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    data_memory
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_DATA            (NB_DATA),
        .RAM_SIZE           (RAM_SIZE) 
    )
    u_data_memory
    (
        .i_clk              (clk),
        .i_wr               (i_wr),
        .i_rd               (i_rd),
        .i_addr             (i_addr),
        .i_data             (i_data),
        .o_data             (o_data)
    );
endmodule
