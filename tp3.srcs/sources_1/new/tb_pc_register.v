`timescale 1ns / 1ps
module tb_pc_register;

	// Inputs
	reg clk;
	reg enable;
	reg reset;
	reg [3:0] pc_in; //d

	// Outputs
	wire [3:0] pc_out;//q

	// Instantiate the Unit Under Test (UUT)
	pc_register uut (
		.i_clk(clk), 
		.i_enable(enable), 
		.i_reset(reset), 
		.i_pc(pc_in), 
		.o_pc(pc_out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		enable = 1;
		reset = 0;
		pc_in = 4'b1010;
		
		#25
		reset = 1;
		#10
		reset = 0;
	end
	
	always begin //clock de la placa 50Mhz
		#10 clk=~clk;
	end

endmodule
