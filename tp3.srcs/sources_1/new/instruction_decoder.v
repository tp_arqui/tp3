module instruction_decoder
#(
	    parameter 	NB_OPCODE     = 5,      //number of bits operation code
	                NB_SELECT_A    = 2,
	                NB_CODE        = 6    	
			        
)
(
		input wire [NB_OPCODE-1:0]      i_opcode,
		output reg                      o_wrPC,
		output reg [NB_SELECT_A-1 : 0]  o_selA,
		output reg                      o_selB,
		output reg                      o_wrACC,
		output reg [NB_CODE-1:0]        o_op,
		output reg                      o_wrRAM,
		output reg                      o_rdRAM
);

    // LOCAL_PARAMETERS
    localparam  ADD_ALU = 6'b100000;
    localparam  SUB_ALU = 6'b100010;
    
    localparam  HLT = 5'b00000;
    localparam  STO = 5'b00001;
    localparam  LD  = 5'b00010;
    localparam  LDI = 5'b00011;
    localparam  ADD = 5'b00100;  
    localparam  ADDI = 5'b00101;
    localparam  SUB = 5'b00110; 
    localparam  SUBI = 5'b00111;  

always @(*)
	case (i_opcode)
		HLT: //HLT                   //En esta operacion se setea todo a cero
			begin
				o_wrPC=0;                     
				o_selA=2'b00;
				o_selB=0;
				o_wrACC=0;
				o_op=0;
				o_wrRAM=0;
				o_rdRAM=0;
			end
		STO: //STO                   
			begin
				o_wrPC=1;                     
				o_selA=2'b00;             //Guarda lo que este en ACC en la memoria. Por eso muxA y muxB son cero
				o_selB=0;
				o_wrACC=0;                //El ACC no se sobrescribe
				o_op=0;                   //No se opera
				o_wrRAM=1;                //Se escribe la memoria
				o_rdRAM=0;
			end
		LD: //LD
			begin
				o_wrPC=1;                 
				o_selA=2'b00;             //Se carga un valor de memoria en ACC. Por eso muxA y muxB son cero
				o_selB=0;
				o_wrACC=1;                //Se escribe el ACC
				o_op=0;
				o_wrRAM=0;
				o_rdRAM=1;                //Se lee la memoria
			end
		LDI: //LDI
			begin
				o_wrPC=1;
				o_selA=2'b01;             //Se carga el operando de la instruccion que viene del decoder en el ACC. Por eso muxA = 1 y muxB = 0   
				o_selB=0;                 
				o_wrACC=1;                //Se escribe el ACC
				o_op=0;                   //No se realiza ninguna operacion
				o_wrRAM=0;                //No se escribe ni lee la memoria
				o_rdRAM=0;    
			end
		ADD: //ADD
			begin
				o_wrPC=1;
				o_selA=2'b10;             //Se suma el valor de la direccion de memoria que sale del decoder con lo que esta en ACC 
				o_selB=0;                 //Por eso muxA = 2 y muxB = 0    
				o_wrACC=1;                //Se escribe el ACC con el resultado de la operacion
				o_op=ADD_ALU;             //Se activa la ALU    
				o_wrRAM=0;
				o_rdRAM=1;                //Se lee la memoria
			end
		ADDI: //ADDI
			begin
				o_wrPC=1;
				o_selA=2'b10;             //Se suma el valor del operando que viene del decoder con lo que esta en el ACC.
				o_selB=1;                 //Por eso muxA = 2 y muxB = 1    
				o_wrACC=1;                //Se escribe el resultado en ACC
				o_op=ADD_ALU;             //Se activa la ALU
				o_wrRAM=0;                //No se lee ni escribe la memoria
				o_rdRAM=0;
			end
		SUB: //SUB               //Funciona igual que la suma
			begin
				o_wrPC=1;
				o_selA=2'b10;
				o_selB=0;
				o_wrACC=1;
				o_op=SUB_ALU;
				o_wrRAM=0;
				o_rdRAM=1;
			end
		SUBI: //SUBI              //Funciona igual que la suma
			begin
				o_wrPC=1;
				o_selA=2'b10;
				o_selB=1;
				o_wrACC=1;
				o_op=SUB_ALU;
				o_wrRAM=0;
				o_rdRAM=0;
			end
		default:
			begin
				o_wrPC=0;
				o_selA=2'b00;
				o_selB=0;
				o_wrACC=0;
				o_op=0;
				o_wrRAM=0;
				o_rdRAM=0;
			end
	endcase	

endmodule
