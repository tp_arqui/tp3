`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 07:45:17 PM
// Design Name: 
// Module Name: mux_B
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module mux_B
#(
    parameter               NB_INSTRUCTION  =   16,                       // Longitud de la instruccion
    parameter               NB_SELECT       =   1                         // Longitud del select
)
(
    // INPUTS   
    input wire [NB_SELECT-1:0]              i_select,
    input wire [NB_INSTRUCTION-1:0]         i_dm,
    input wire [NB_INSTRUCTION-1:0]         i_se,
    
    // OUTPUTS
	output wire [NB_INSTRUCTION-1:0]        o_mux
);

    // LOCAL_PARAMETERS
    localparam  DM = 1'b0;          // Data memory
    localparam  SE = 1'b1;          // Signal extension
    
    // INTERNAL
    reg         [NB_INSTRUCTION-1:0]       aux = 0; 
    
    always @(*) begin
        case (i_select)
            DM: aux <= i_dm;
            SE: aux <= i_se;
            default: aux = 0;
        endcase
    end
    
    // OUTPUT
    assign o_mux = aux;
    
endmodule