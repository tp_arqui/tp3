`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 09:49:07 PM
// Design Name: 
// Module Name: tb_mux_B
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_mux_B();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_SELECT        = 1;
        
    localparam  DM              = 1'b0;          // Data memory
    localparam  SE              = 1'b1;          // Signal extension
    localparam  DM_INPUT        = 16'h07AA;
    localparam  SE_INPUT        = 16'hFFAA;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         test_start;
    reg                         bool;
    // MUX_A
    reg [NB_SELECT-1:0]         i_select;
    reg [NB_INSTRUCTION-1:0]    i_dm;
    reg [NB_INSTRUCTION-1:0]    i_se;
    wire [NB_INSTRUCTION-1:0]   o_mux;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        bool = 1'b0;
        
        #10
        i_dm = DM_INPUT;
        i_se = SE_INPUT;
        i_select = DM;
        
        #10
        test_start = 1'b1;
        
        #100
        i_select = SE;
    
        #100
        
        #1000
        
        $display("############# Test OK ############");
        $finish();
                
    end
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    mux_B
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_SELECT          (NB_SELECT)
    )
    u_mux_B
    (
        .i_select           (i_select),
        .i_dm               (i_dm),
        .i_se               (i_se),
        .o_mux              (o_mux)
    );
    
    // TEST
    always @(posedge clk) begin       
        if (test_start) begin
            case (i_select)
                DM: bool = (o_mux != DM_INPUT);
                SE: bool = (o_mux != SE_INPUT);
                default: bool = 1'b1;
            endcase
            if(bool) begin
                $display("############# Test FALLO ############");
                $finish();
            end
        end        
    end
          
endmodule