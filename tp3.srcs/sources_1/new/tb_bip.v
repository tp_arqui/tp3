`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/24/2020 08:40:08 PM
// Design Name: 
// Module Name: tb_bip
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_bip();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_OPERAND       = NB_INSTRUCTION-NB_OPCODE;        
    localparam NB_SELECT_A      = 2;
    localparam NB_SELECT_B      = 1;
    localparam NB_CODE          = 6;
    localparam NB_DATA          = NB_INSTRUCTION;
    localparam NB_ADDRESS       = 4;
    localparam RAM_SIZE         = NB_INSTRUCTION;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         test_start;
    reg                         reset;
    // ACC_OUT
    wire [NB_INSTRUCTION-1:0]   o_acc;
    wire                        tx_start;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        reset = 1'b0;
        
        #10
        reset = 1'b1;
        
        #10
        reset = 1'b0;      
        
        #250
        
        $display("############# Test OK ############");
        $finish();
                
    end
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    bip
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A),
        .NB_SELECT_B        (NB_SELECT_B),
        .NB_CODE            (NB_CODE),
        .NB_DATA            (NB_DATA),
        .RAM_SIZE           (RAM_SIZE),
        .NB_ADDRESS         (NB_ADDRESS)
    )
    u_bip
    (
        .i_clk              (clk),
        .i_reset            (reset),
        .o_acc              (o_acc),
        .o_tx_start         (tx_start)
    );
      
endmodule
