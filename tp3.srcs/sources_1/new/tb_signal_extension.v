`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/19/2020 08:30:04 PM
// Design Name: 
// Module Name: tb_signal_extension
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_signal_extension();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_OPERAND       = NB_INSTRUCTION - NB_OPCODE;
    localparam SIGNAL_A         = 11'h7AA;
    localparam SIGNAL_B         = 11'h3AA;
    
    localparam EXPECTED_OUT_A   = 16'hFFAA;
    localparam EXPECTED_OUT_B   = 16'h03AA;
    
    // TB_SIGNALS
    reg                         clk;
    reg                         test_start;
    // SIGNAL_EXTENSION
    reg [NB_OPERAND-1:0]        i_se;
    wire [NB_INSTRUCTION-1:0]   o_se;
    
    initial begin
        clk = 1'b0;
        test_start = 1'b0;
        
        #10
        i_se = SIGNAL_A;
        
        #10
        test_start = 1'b1;
               
        #100
        i_se = SIGNAL_B;
        
        #1000
        
        $display("############# Test OK ############");
        $finish();
                
    end
    
    
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    signal_extension
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND)
    )
    u_signal_extension
    (
        .i_se               (i_se),
        .o_se               (o_se)
    );
    
    // TEST
    always @(posedge clk) begin       
        if (test_start)
            if ((i_se == SIGNAL_A) && (o_se != EXPECTED_OUT_A)) begin
                $display("############# Test FALLO ############");
                $finish();
            end
            else if ((i_se == SIGNAL_B) && (o_se != EXPECTED_OUT_B)) begin
                $display("############# Test FALLO ############");
                $finish();
            end
    end
          
endmodule
