`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/23/2020 06:56:02 PM
// Design Name: 
// Module Name: tb_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_control();

    // LOCAL_PARAMETERS
    localparam NB_INSTRUCTION   = 16;
    localparam NB_OPCODE        = 5;
    localparam NB_OPERAND       = NB_INSTRUCTION-NB_OPCODE;        
    localparam NB_SELECT_A      = 2;
    localparam NB_CODE          = 6;
    
    
    // TB_SIGNALS
    reg                             clk;
    reg                             test_start;
    
    //CONTROL_SIGNALS
    reg [NB_INSTRUCTION - 1:0]      i_pm_data;
	reg                             i_reset_pc;
	wire [NB_SELECT_A - 1:0]        o_selA;
    wire                            o_selB;
    wire                            o_wrACC;
    wire [NB_CODE-1:0]              o_op;
    wire                            o_wrRAM;
    wire                            o_rdRAM;
    wire [NB_OPERAND - 1:0]         o_operand; 
    wire [3:0]                      o_address; 
    
    //AUX
    localparam  ADD_ALU = 6'b100000;
    localparam  SUB_ALU = 6'b100010;
    
    //Programa en la memoria. Simula ser el program memory.
    localparam DATA_1           = 16'h180A; //LDI 10; ACC<-10
    localparam DATA_2           = 16'h0800; //STO A ;A <- ACC;
    localparam DATA_3           = 16'h1814; //LDI 20;  ACC<-20
    localparam DATA_4           = 16'h0801; //STO B; B <- ACC
    localparam DATA_5           = 16'h1000; //LD A; ACC<- A 
    localparam DATA_6           = 16'h2001; //ADD B; ACC<-ACC+B
    localparam DATA_7           = 16'h3805; //SUBI 5; ACC<-ACC-5
    localparam DATA_8           = 16'h0802; //STO C; C<-ACC                
    localparam DATA_9           = 16'h1FF1; // LDI -15; ACC<- -15  
    localparam DATA_10          = 16'h3002; //	SUB C; ACC<-ACC-C
    localparam DATA_11          = 16'h0000; //	No hay mas programa a partir de esta direccion
    
    //El test consiste en ejecutar instrucciones como si las estuviese leyendo del program memory
    //Luego se testea que las se;ales sean las correctas, dependiendo de la operacion. 
    //Ademas, que la direccion de salida, sea la correcta, para ver que el Program Counter funciona bien. 
    initial 
        begin
              clk = 1'b0;
              test_start = 1'b0;
              i_reset_pc = 1'b0;
              i_pm_data  = DATA_11;
              
              #10
              test_start = 1'b1;
              i_reset_pc = 1'b1;
        
              #10
              i_reset_pc = 1'b0;
              
                    
              #10
              i_pm_data = DATA_1;
              
              #20
              //Chekeco que la operacion anterior funciono. 
              if(o_address != 4'b0000 || o_selA != 2'b01 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                  begin
                         $display("############# Test FALLO ############");
                         $finish();
                  end
              //Cargo nueva operacion 
              i_pm_data = DATA_2;
              
              #20
              //Chekeco que la operacion anterior funciono
               if(o_address != 4'b0001 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 1 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_3;
              
              #20
              //Chekeco que la operacion anterior funciono
              if(o_address != 4'b0010 || o_selA != 2'b01 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                  begin
                         $display("############# Test FALLO ############");
                         $finish();
                  end
              //Cargo nueva operacion                     
              i_pm_data = DATA_4;
              
              #20
              //Chekeco que la operacion anterior funciono
               if(o_address != 4'b0011 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 1 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_5;
              
              #20
              //Chekeco que la operacion anterior funciono
               if(o_address != 4'b0100 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_6;
              
              #20
              //Chekeco que la operacion anterior funciono
              if(o_address != 4'b0101 || o_selA != 2'b10 || o_selB != 0 || o_wrACC != 1 || o_op != ADD_ALU || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_7;
              
              #20
              //Chekeco que la operacion anterior funciono
               if(o_address != 4'b0110 || o_selA != 2'b10 || o_selB != 1 || o_wrACC != 1 || o_op != SUB_ALU || o_wrRAM != 0 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_8;
              
              #20
              //Chekeco que la operacion anterior funciono
              if(o_address != 4'b0111 || o_selA != 2'b00 || o_selB != 0 || o_wrACC != 0 || o_op != 0 || o_wrRAM != 1 || o_rdRAM != 0)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
              //Cargo nueva operacion       
              i_pm_data = DATA_9;
              
              #20
              //Chekeco que la operacion anterior funciono. 
              if(o_address != 4'b1000 || o_selA != 2'b01 || o_selB != 0 || o_wrACC != 1 || o_op != 0 || o_wrRAM != 0 || o_rdRAM != 0)
                  begin
                         $display("############# Test FALLO ############");
                         $finish();
                  end
              //Cargo nueva operacion 
              i_pm_data = DATA_10;
              
              #20
              //Chekeco que la operacion anterior funciono
              if(o_address != 4'b1001 || o_selA != 2'b10 || o_selB != 0 || o_wrACC != 1 || o_op != SUB_ALU || o_wrRAM != 0 || o_rdRAM != 1)
                    begin
                         $display("############# Test FALLO ############");
                         $finish();
                    end
             //Cargo nueva operacion 
              i_pm_data = DATA_11;
              
        end
        
    // CLOCK_GENERATION
    always #10 clk = ~clk;
    
    control
    #(
        .NB_INSTRUCTION     (NB_INSTRUCTION),
        .NB_OPCODE          (NB_OPCODE),
        .NB_OPERAND         (NB_OPERAND),
        .NB_SELECT_A        (NB_SELECT_A)
    )
    u_control
    (
        .i_pm_data          (i_pm_data), 
        .i_clk              (clk),
        .i_reset_pc         (i_reset_pc),
        .o_selA             (o_selA),
        .o_selB             (o_selB),
        .o_wrACC            (o_wrACC),
        .o_op               (o_op),
        .o_wrRAM            (o_wrRAM),
        .o_rdRAM            (o_rdRAM),
        .o_operand          (o_operand), 
        .o_address          (o_address)
    );
endmodule
