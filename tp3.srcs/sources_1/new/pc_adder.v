module pc_adder(
	input wire[3:0] i_pc,
	output reg[3:0] o_pcadd
);

always @(*) 
begin
	o_pcadd=i_pc+1;  //Suma uno a la direccion de entrada y lo pone a la salida
end
	
endmodule
